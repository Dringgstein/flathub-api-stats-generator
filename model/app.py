"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from . import BaseModel


class AppModel(BaseModel):
    NAME = "App Model"

    def __init__(self, api, extra={}):
        BaseModel.__init__(self, api, extra)

        self.apps = {}

    def __len__(self):
        return len(self.apps)

    def load_generator(self):
        for i, app_record in enumerate(self.api.get_apps()):
            app_id = app_record[self.api.FIELD_ID]
            app = self.api.get_app_by_record(app_record)

            # Take the info we care about and store to the apps store
            self.apps[app_id] = {
                "id": app_id,
                "creationDate": self.api.parse_date(
                    app[self.api.FIELD_CREATION_DATE]
                ),
            }

            yield i

    def apps_ordered_by(self, field):
        return sorted(self.apps.values(), key=lambda a: a[field])
