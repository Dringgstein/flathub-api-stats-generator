"""
Copyright (C) 2018
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from re import compile as re_compile

from model import BaseModel


IS_RUNTIME = re_compile(
    r"^(org\..*\.(Base)?(Platform|Sdk)\.?.*|.*\..*\.(Gtk3theme|KStyle)\.?.*|.*\..*\..*\.BaseApp\.?.*)$"
)
IS_NVIDIA = re_compile(
    r"^(org\.freedesktop\.Platform\.GL\.nvidia-){1}(\d+\-\d+){1}\/(\d+\.\d+){1}$"
)
IS_NVIDIA_32 = re_compile(
    r"^(org\.freedesktop\.Platform\.GL32\.nvidia-){1}(\d+\-\d+){1}\/(\d+\.\d+){1}$"
)


class BaseReport():
    HAS_DATA_STRING = False
    HAS_DATA_ZIPPED = False
    MODEL = BaseModel

    # We use args=[] so that the same key can be repeated, eg app-ud=org.gnome.Recipes,app-id=org.gnome.gedit
    def __init__(self, model, args=[]):
        self.model = model

    @property
    def axis(self):
        return ["X", "Y"]

    # Data as str line for dat output, ["x1 y1", "x2 y2", "x3 y3"]
    def data_string(self):
        return []

    # Zipped data for graphs, [(x1, y1), (x2, y2), (x3, y3)]
    def data_zipped(self):
        return []

    # kwargs for zipped data, eg x_date=True
    def data_zipped_kwargs(self):
        return []

    @property
    def title(self):
        return "Title"


from .app_count_by_creation_date import AppCountByCreationDateReport
from .apps_by_creation_date import AppsByCreationDateReport
from .apps_by_name import AppsByNameReport
from .downloads_app_by_date import DownloadsAppByDateReport
from .downloads_arch_by_date import DownloadsArchByDateReport
from .downloads_by_date import DownloadsByDateReport
from .downloads_flatpak_major_version_by_date import DownloadsFlatpakMajorVersionByDateReport
from .downloads_flatpak_version_by_date import DownloadsFlatpakVersionByDateReport
from .downloads_only_apps_by_date import DownloadsOnlyAppsByDateReport
from .downloads_only_nvidia_runtimes_by_date import DownloadsOnlyNvidiaRuntimesByDateReport
from .downloads_only_runtimes_by_date import DownloadsOnlyRuntimeByDateReport
from .downloads_os_version_by_date import DownloadsOSTreeVersionByDateReport
