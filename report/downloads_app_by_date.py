"""
Copyright (C) 2018, 2020
     Andrew Hayzen <ahayzen@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime

from . import BaseReport
from model import DownloadModel


def chain(a, b):
    return [j for i in zip(a, b) for j in i]


# FIXME: the graph in this report is not of good quality, we need to limit to eg 10 apps
class DownloadsAppByDateReport(BaseReport):
    HAS_DATA_STRING = True
    HAS_DATA_ZIPPED = True
    MODEL = DownloadModel

    def __init__(self, model, args=[]):
        BaseReport.__init__(self, model)

        self.apps = []
        self.period = 'daily'

        for arg in args:
            try:
                key, value = arg
            except ValueError:
                print("Expected exactly one equals, got these parts: %s" % arg)
                break

            if key == "period":
                if value in ['daily', 'weekly', 'monthly']:
                    self.period = value
            elif key == "app-id":
                self.apps.append(value)
            else:
                print("Unknown report-arg %s=%s" % (key, value))

        if len(self.apps) == 0:
            # Pre read all the apps
            for _, data in self.model.downloads_by_date():
                for app in data["refs"]:
                    if app not in self.apps:
                        self.apps.append(app)

    @property
    def axis(self):
        axis_name = "Time"
        if self.period == 'weekly':
            axis_name = "Time (ISO Week Numbers)"
        if self.period == 'monthly':
            axis_name = "Time (Calendar Months)"
        return [axis_name, "Downloads"]

    def data_string(self):
        return [
            "%s\t%i\t%i" % (
                app,
                sum(
                  sum(data["refs"][app][arch][0] for arch in data["refs"].get(app, []))
                  for date, data in self.model.downloads_by_date()
                ),
                sum(
                  sum(data["refs"][app][arch][1] for arch in data["refs"].get(app, []))
                  for date, data in self.model.downloads_by_date()
                )
            )
            for app in self.apps
        ]

    def data_zipped(self):
        app_totals = {}
        for app in self.apps:
            period_totals = {}
            for data_date, data in self.model.downloads_by_date():
                p = data_date
                if self.period == 'monthly':
                    p = f"{data_date.year}-{data_date.month:02}"
                if self.period == 'weekly':
                    year, week, _ = data_date.isocalendar()
                    p = f"{year} W{week:02}"
                if p not in period_totals:
                    period_totals[p] = [0, 0]
                # All architecture download totals by date
                a = sum(data["refs"][app][arch][0] for arch in data["refs"].get(app, []))
                b = sum(data["refs"][app][arch][1] for arch in data["refs"].get(app, []))
                # Add to totals for current period
                period_totals[p][0] = period_totals[p][0] + a
                period_totals[p][1] = period_totals[p][1] + b
            app_totals[app] = period_totals
        return chain(
            [
                zip(
                    *(
                        [date, data[0]]
                        for date, data in app_totals[app].items()
                    )
                ) for app in self.apps
            ],
            [
                zip(
                    *(
                        [date, data[1]]
                        for date, data in app_totals[app].items()
                    )
                ) for app in self.apps
            ],
        )

    def data_zipped_kwargs(self):
        return chain(
            [
                {
                    "label": app,
                    "x_date": True,
                } for app in self.apps
            ],
            [
                {
                    "label": app + " (Delta Downloads)",
                    "x_date": True,
                } for app in self.apps
            ],
        )

    @property
    def title(self):
        return (
            "%s downloads by app over time - %s" % (
                self.period.title(),
                datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S")
            )
        )
